package cn.jimu98.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.jimu98.common.utils.PageUtils;
import cn.jimu98.gulimall.coupon.entity.HomeSubjectSpuEntity;

import java.util.Map;

/**
 * 专题商品
 *
 * @author jimu98
 * @email z591593455@qq.com
 * @date 2020-07-22 15:12:25
 */
public interface HomeSubjectSpuService extends IService<HomeSubjectSpuEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

