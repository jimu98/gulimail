package cn.jimu98.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.jimu98.common.utils.PageUtils;
import cn.jimu98.gulimall.coupon.entity.SeckillPromotionEntity;

import java.util.Map;

/**
 * 秒杀活动
 *
 * @author jimu98
 * @email z591593455@qq.com
 * @date 2020-07-22 15:12:25
 */
public interface SeckillPromotionService extends IService<SeckillPromotionEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

