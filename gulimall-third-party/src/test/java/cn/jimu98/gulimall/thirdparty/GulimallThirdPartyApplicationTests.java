package cn.jimu98.gulimall.thirdparty;

import com.aliyun.oss.OSSClient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

@SpringBootTest
class GulimallThirdPartyApplicationTests {
    @Autowired
    OSSClient ossClient;

    @Test
    public void test() throws FileNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("C:\\Users\\kaibo\\Pictures\\5e95539f891d1.jpg");
        ossClient.putObject("gulimall-jimu98", "aaa.jpg", fileInputStream);
        ossClient.shutdown();
        System.out.println("上传完成");
    }
    @Test
    void contextLoads() {
    }

}
