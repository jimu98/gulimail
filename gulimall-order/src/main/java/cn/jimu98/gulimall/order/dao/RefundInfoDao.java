package cn.jimu98.gulimall.order.dao;

import cn.jimu98.gulimall.order.entity.RefundInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 退款信息
 * 
 * @author jimu98
 * @email z591593455@qq.com
 * @date 2020-07-22 15:28:47
 */
@Mapper
public interface RefundInfoDao extends BaseMapper<RefundInfoEntity> {
	
}
