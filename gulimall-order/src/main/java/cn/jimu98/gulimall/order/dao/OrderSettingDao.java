package cn.jimu98.gulimall.order.dao;

import cn.jimu98.gulimall.order.entity.OrderSettingEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单配置信息
 * 
 * @author jimu98
 * @email z591593455@qq.com
 * @date 2020-07-22 15:28:48
 */
@Mapper
public interface OrderSettingDao extends BaseMapper<OrderSettingEntity> {
	
}
