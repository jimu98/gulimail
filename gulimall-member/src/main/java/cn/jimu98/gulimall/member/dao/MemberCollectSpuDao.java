package cn.jimu98.gulimall.member.dao;

import cn.jimu98.gulimall.member.entity.MemberCollectSpuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的商品
 * 
 * @author jimu98
 * @email z591593455@qq.com
 * @date 2020-07-22 15:20:20
 */
@Mapper
public interface MemberCollectSpuDao extends BaseMapper<MemberCollectSpuEntity> {
	
}
