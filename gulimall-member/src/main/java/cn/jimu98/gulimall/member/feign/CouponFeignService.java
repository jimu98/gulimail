package cn.jimu98.gulimall.member.feign;

import cn.jimu98.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Classname CouponFeignService
 * @Description TODO
 * @Date 2020/7/22 16:34
 * @Created by kaibo
 */
@FeignClient("gulimall-coupon")
public interface CouponFeignService {
    @RequestMapping("coupon/coupon/member/list")
    public R memberCoupons();
}
