package cn.jimu98.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.jimu98.common.utils.PageUtils;
import cn.jimu98.gulimall.member.entity.MemberStatisticsInfoEntity;

import java.util.Map;

/**
 * 会员统计信息
 *
 * @author jimu98
 * @email z591593455@qq.com
 * @date 2020-07-22 15:20:21
 */
public interface MemberStatisticsInfoService extends IService<MemberStatisticsInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

