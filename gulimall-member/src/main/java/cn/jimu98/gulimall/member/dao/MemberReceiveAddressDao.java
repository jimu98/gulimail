package cn.jimu98.gulimall.member.dao;

import cn.jimu98.gulimall.member.entity.MemberReceiveAddressEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收货地址
 * 
 * @author jimu98
 * @email z591593455@qq.com
 * @date 2020-07-22 15:20:21
 */
@Mapper
public interface MemberReceiveAddressDao extends BaseMapper<MemberReceiveAddressEntity> {
	
}
