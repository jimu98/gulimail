package cn.jimu98.gulimall.ware.dao;

import cn.jimu98.gulimall.ware.entity.PurchaseDetailEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author jimu98
 * @email z591593455@qq.com
 * @date 2020-07-22 15:39:59
 */
@Mapper
public interface PurchaseDetailDao extends BaseMapper<PurchaseDetailEntity> {
	
}
