package cn.jimu98.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.jimu98.common.utils.PageUtils;
import cn.jimu98.gulimall.product.entity.CategoryBrandRelationEntity;

import java.util.Map;

/**
 * 品牌分类关联
 *
 * @author jimu98
 * @email z591593455@qq.com
 * @date 2020-07-22 15:10:38
 */
public interface CategoryBrandRelationService extends IService<CategoryBrandRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveDetail(CategoryBrandRelationEntity categoryBrandRelation);

    void updateBrand(Long brandId, String name);

    void updateCategory(Long catId, String name);
}

