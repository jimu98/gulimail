package cn.jimu98.gulimall.product.dao;

import cn.jimu98.gulimall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author jimu98
 * @email z591593455@qq.com
 * @date 2020-07-22 15:10:38
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
