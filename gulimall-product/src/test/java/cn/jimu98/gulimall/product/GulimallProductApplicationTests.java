package cn.jimu98.gulimall.product;

import cn.jimu98.gulimall.product.entity.BrandEntity;
import cn.jimu98.gulimall.product.service.BrandService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

@SpringBootTest
class GulimallProductApplicationTests {


    @Autowired
    BrandService brandService;


//
//    @Test
//    public void test() throws FileNotFoundException {
//        FileInputStream fileInputStream = new FileInputStream("C:\\Users\\kaibo\\Pictures\\5e95539f891d1.jpg");
//        ossClient.putObject("gulimall-jimu98", "aaa.jpg", fileInputStream);
//        ossClient.shutdown();
//        System.out.println("上传完成");
//    }
    @Test
    void contextLoads() {
        BrandEntity brandEntity = new BrandEntity();
        brandEntity.setName("华为");

        brandService.save(brandEntity);
        System.out.println("保存成功");
    }

}
